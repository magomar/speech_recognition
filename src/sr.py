import speech_recognition as sr
print(f'SpeechRecognition version {sr.__version__}')

r = sr.Recognizer()

# Consider CMU Sphinx for offline speech recognition (https://cmusphinx.github.io/)

#  Where to get free audio samples:
#  https://samplefocus.com/categories/spoken
#  http://www.voiptroubleshooter.com/open_speech/index.html

# files = ['data/parts-of-the-body.wav', 'data/flashing-in-the-sun.wav']
# for file in files:
#     audio = sr.AudioFile(file)
#     with audio as source:
#         audio = r.record(source)
#     transcription = r.recognize_google(audio)
#     print(transcription)

# harvard = sr.AudioFile('data/harvard.wav')
# with harvard as source:
#     audio = r.record(source)
# print(r.recognize_google(audio))

# with harvard as source:
#     audio1 = r.record(source, offset=4, duration=3)
#     audio2 = r.record(source, duration=3)
# print(r.recognize_google(audio1))
# print(r.recognize_google(audio2))

# With noisy audio
jackhammer = sr.AudioFile('data/jackhammer.wav')
with jackhammer as source:
    r.adjust_for_ambient_noise(jackhammer, duration=0.5)
    audio=r.record(source)
print(r.recognize_google(audio))


#  Digital Signal Processing: https://greenteapress.com/wp/think-dsp/

